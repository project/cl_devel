<?php

namespace Drupal\cl_devel\Controller;

use Drupal\cl_components\Component\ComponentMetadata;
use Drupal\cl_components\ComponentPluginManager;
use Drupal\cl_components\Exception\ComponentSyntaxException;
use Drupal\cl_components\Exception\InvalidComponentException;
use Drupal\cl_components\ExtensionType;
use Drupal\cl_components\Parser\ComponentPhpFile;
use Drupal\cl_components\Plugin\Component;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the registry.
 */
final class ComponentAudit extends ControllerBase {

  /**
   * @param \Drupal\cl_components\ComponentPluginManager $pluginManager
   */
  public function __construct(private readonly ComponentPluginManager $pluginManager) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $plugin_manager = $container->get('plugin.manager.cl_component');
    assert($plugin_manager instanceof ComponentPluginManager);
    return new static($plugin_manager);
  }

  /**
   * Render the registry.
   *
   * @return array
   *   The render array.
   *
   * @throws \Drupal\cl_components\Exception\ComponentNotFoundException
   */
  public function audit(): array {
    $components = $this->pluginManager->getAllComponents();
    // We can only know for certain what components are forked if they are in
    // a module.
    $module_components = array_filter(
      $components,
      static fn(Component $component) => ($component->getPluginDefinition()['extension_type'] ?? '') === ExtensionType::Module
    );
    $duped_components = array_filter(
      $module_components,
      fn(Component $component) => ($component->getPluginId()) !== ($this->pluginManager->find($component->getId())
        ->getPluginId())
    );
    $build_component_card = fn(Component $component) => $this->buildComponentCard($component, $duped_components);
    usort($components, static fn(Component $a, Component $b) => $a->getId() <=> $b->getId());
    return [
      'logs' => [
        '#prefix' => '<p>',
        '#markup' => $this->t('Remember to check your logs if you are having problems with your components.'),
        '#suffix' => '</p>',
      ],
      'components' => [
        '#type' => 'container',
        '#attributes' => ['class' => 'panel__container'],
        '#attached' => ['library' => ['cl_devel/cl_registry']],
        'panels' => [
          '#theme' => 'item_list',
          '#title' => $this->t('Detected Components'),
          '#items' => array_map($build_component_card, $components),
        ],
        'recommendations' => [
          '#prefix' => '<p>',
          '#markup' => $this->t('We recommend you to add a <code>README.md</code> file and a <code>thumbnail.png</code> to all your components. Other modules and JS tools may use them to provide additional insight.
'),
          '#suffix' => '</p>',
        ],
      ],
    ];
  }

  /**
   * Builds the render array for the component card.
   *
   * @param \Drupal\cl_components\Plugin\Component $component
   *   The current component.
   * @param \Drupal\cl_components\Plugin\Component[] $duped_components
   *   The components without duplications (for forked components).
   *
   * @return array[]
   *   The card render array.
   */
  private function buildComponentCard(Component $component, array $duped_components): array {
    $metadata = $component->getMetadata();
    $path = $metadata->getPath();
    try {
      $matches = array_filter($duped_components, static fn(Component $cmp) => $cmp->getId() === $component->getId());
      $match = reset($matches);
      $is_forked = $match && $match->getMetadata()->getPath() === $path;
    }
    catch (InvalidComponentException $e) {
      $component = NULL;
      $message = $this->t('Invalid component @path. Error: @error', [
        '@path' => $path,
        '@error' => $e->getMessage(),
      ]);
      return [
        'title' => [
          '#prefix' => '<h4>',
          '#markup' => $this->t('💥 Error in Component'),
          '#suffix' => '</h4>',
        ],
        'path' => [
          '#prefix' => '<pre>',
          '#markup' => $path,
          '#suffix' => '</pre>',
        ],
        'error' => [
          '#prefix' => '<p>',
          '#markup' => $e->getMessage(),
          '#suffix' => '</p>',
        ],
        '#wrapper_attributes' => ['class' => 'panel-item'],
      ];
    }
    $default_template = $component->getId() . '.twig';
    $template_message = in_array($default_template, $component->getTemplates())
      ? $this->t('✅ %default template is present', ['%default' => $default_template])
      : $this->t('❌ @id.twig is missing', ['@id' => $component->getId()]);
    $missing_variants = array_diff(
      [
        $default_template,
        ...array_map(static fn(string $variant) => sprintf('%s--%s.twig', $component->getId(), $variant), $component->getVariants()),
      ],
      $component->getTemplates()
    );
    $missing_variants = array_filter(
      $missing_variants,
      static fn(string $name) => $name !== $default_template
    );
    $variants_message = empty($missing_variants)
      ? $this->t('✅ All variants have their templates')
      : $this->t('❌ Missing templates: %variants', ['%variants' => implode(', ', $missing_variants)]);
    $assets = $component->getPluginDefinition()['libraryFiles'] ?? [];
    $assets_message = [
      '#theme' => 'item_list',
      '#items' => array_map(
        static fn(string $file) => [
          '#type' => 'html_tag',
          '#tag' => 'code',
          '#value' => preg_replace('@.*/' . $component->getId() . '/@', '', $file),
        ],
        [
          ...$assets['js'] ?? [],
          ...$assets['css'] ?? [],
        ]
      ),
    ];
    if (!$is_forked) {
      try {
        $hooks = $component->getMetadata()->getHooks();
      }
      catch (ComponentSyntaxException $e) {
        $hooks = [];
      }
      $hooks_message = [
        '#theme' => 'item_list',
        '#items' => array_map(
          static fn(string $hook) => [
            '#type' => 'html_tag',
            '#tag' => 'code',
            '#value' => $hook,
            '#attributes' => ['class' => in_array($hook, $hooks, TRUE) ? 'implemented' : 'not-implemented'],
          ],
          ComponentPhpFile::SUPPORTED_CL_HOOKS
        ),
      ];
    }
    else {
      $hooks_message = [
        '#type' => 'html_tag',
        '#tag' => 'em',
        '#value' => $this->t('Forked at <code>@filename</code>.', [
          '@filename' => $this->pluginManager->find($component->getId())
            ->getMetadata()
            ->getPath(),
        ]),
      ];
    }
    $card_build = [
      'title' => [
        '#theme' => 'cl_label_with_link',
        '#label' => $metadata->getName() . ($is_forked ? ' ❗' : ''),
        '#href' => '#' . $component->getPluginId(),
      ],
      'description' => $metadata
        ->getDescription() === ComponentMetadata::DEFAULT_DESCRIPTION ? [] : [
          '#prefix' => '<p>',
          '#markup' => $metadata->getDescription(),
          '#suffix' => '</p>',
        ],
      'path' => [
        '#prefix' => '<pre>',
        '#markup' => $path,
        '#suffix' => '</pre>',
      ],
      'forked' => $is_forked ? [
        '#markup' => $this->t('When rendering this component by ID, this other component will render instead: <code>@filename</code>.', [
          '@filename' => $this->pluginManager->find($component->getId())
            ->getMetadata()
            ->getPath(),
        ]),
      ] : [],
      'table' => [
        '#theme' => 'table',
        '#header' => [
          $this->t('Metadata'),
          $this->t('Default Template'),
          $this->t('Variants'),
          $this->t('Assets'),
          $is_forked ? $this->t('🍴 Component Forked 🍴') : $this->t('Implemented Hooks'),
        ],
        '#rows' => [
          [
            $this->t('✅ All metadata is correct'),
            $template_message,
            $variants_message,
            ['data' => $assets_message],
            ['data' => $hooks_message],
          ],
        ],
      ],
      '#wrapper_attributes' => [
        'class' => ['panel-item', $is_forked ? 'forked' : 'not-forked'],
        'id' => $component->getPluginId(),
      ],
    ];
    if (!$is_forked) {
      $this->moduleHandler()
        ->alter('cl_component_audit', $card_build, $component);
    }
    return $card_build;
  }

}
